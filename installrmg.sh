#!/bin/bash

declare -A color;
color[default]=9;
color[green]=2;
color[red]=1;
color[yellow]=3;

if [[ 'root' != $(whoami) ]]; then
    tput setaf ${color[red]};
    echo "This command must be run as root.";
    tput setaf ${color[default]};
    exit 1;
fi

if [[ $(tty) == "not a tty" ]]; then
    tput setaf ${color[red]};
    echo "This command must be run with an interactive shell.";
    tput setaf ${color[default]};
    exit 1;
fi

tput setaf ${color[green]};
echo "
Welcome to the RMG Media Puppet VM Setup Script!
";

tput setaf ${color[default]};
echo "This script will set up a VM from scratch as a project VM. The prerequisite
Puppet config must be set up for your project in order for this to work.

We will ask you for some information about you and your project and then you will be on your way.

After you enter your project information, Puppet will run which may take a long time.
Then, you will be prompted to run a command to install the application(s).
";
while [[ -z "$project" ]]; do
    tput setaf ${color[yellow]};
    echo "Project Code (e.g. myproject):"
    tput setaf ${color[default]};
    read project;
    if [[ -z "$project" ]]; then
        tput setaf ${color[red]};
        echo "Project Code must be set.";
        tput setaf ${color[default]};
    fi
    # Ensure project code is all lower case
    project="${project,,}";
done

tput setaf ${color[yellow]};
echo "Puppet Top Level Domain (Just press enter for robofirm.net):"
tput setaf ${color[default]};
read puppet_tld;
puppet_tld=${puppet_tld:-robofirm.net};

tput setaf ${color[yellow]};
echo "Puppetmaster FQDN (Just press enter for newpuppet.robofirm.net):"
tput setaf ${color[default]};
read puppetmaster_domain;
puppetmaster_domain=${puppetmaster_domain:-newpuppet.robofirm.net};


tput setaf ${color[yellow]};
echo "Puppet Environment (Just press enter for local):"
tput setaf ${color[default]};
read puppet_env;
puppet_env=${puppet_env:-local};

while [[ -z "$full_name" ]]; do
    tput setaf ${color[yellow]};
    echo "Your Full Name:";
    tput setaf ${color[default]};
    read full_name;
    if [[ -z "$full_name" ]]; then
        tput setaf ${color[red]};
        echo "Full Name must be set.";
        tput setaf ${color[default]};
    fi
done

while [[ -z "$email" ]]; do
    tput setaf ${color[yellow]};
    echo "Email:";
    tput setaf ${color[default]};
    read email;
    if [[ -z "$email" ]]; then
        tput setaf ${color[red]};
        echo "Email must be set.";
        tput setaf ${color[default]};
    fi
done



bitbucketCredentialsValid=false;
while [[ false == "$bitbucketCredentialsValid" ]]; do
    while [[ -z "$bitbucket_username" ]]; do
        tput setaf ${color[yellow]};
        echo "Bitbucket Username (not email):";
        tput setaf ${color[default]};
        read bitbucket_username;
        if [[ -z "$bitbucket_username" ]]; then
            tput setaf ${color[red]};
            echo "Bitbucket Username must be set.";
            tput setaf ${color[default]};
        fi
    done

#    while [[ -z "$bitbucket_password" ]]; do
#        tput setaf ${color[yellow]};
#        echo "Bitbucket Password:
#";
#        tput setaf ${color[default]};
#        read -s bitbucket_password;
#        if [[ -z "$bitbucket_password" ]]; then
#            tput setaf ${color[red]};
#            echo "Bitbucket Password must be set.";
#            tput setaf ${color[default]};
#        fi
#    done

    while [[ -z "$bitbucket_password" ]]; do
        tput setaf ${color[yellow]};
        echo "Please create an App Password. Visit https://bitbucket.org/account/user/$bitbucket_username/app-passwords"
        echo "Click Create app password."
        echo "Give the app password a name related to the application that will use the password."
        echo "Select the specific access and permissions. Check only Account Email/Read/Write"
        echo "Copy the generated password and record it and copy/paste below. The password is only displayed this one time."
        echo "Bitbucket App Password (not login password):";
        tput setaf ${color[default]};
        read -s bitbucket_password;
        if [[ -z "bitbucket_password" ]]; then
            tput setaf ${color[red]};
            echo "Bitbucket App Password must be set.";
            tput setaf ${color[default]};
        fi
    done
    # Validate Bitbucket credentials
    echo "Validating Bitbucket credentials...";
    httpStatus=$(curl -s -o /dev/null -I -w "%{http_code}"  --user ${bitbucket_username}:${bitbucket_password} https://api.bitbucket.org/2.0/user);
    if [[ 200 == $httpStatus ]]; then
        bitbucketCredentialsValid=true;
    else
        tput setaf ${color[red]};
        echo "Bitbucket credentials failed. Please re-enter.";
        tput setaf ${color[default]};
        unset bitbucket_username
        unset bitbucket_password
    #    unset bitbucket_apppassword
    fi
done

tput setaf ${color[green]};
echo "Thank you. Your VM is now being set up...";

# exiting here for testing
# exit 0

# Setting Hostname
hostnamectl set-hostname local-${project}.${puppet_tld};
echo "127.0.0.1 local-${project} ${project}-local" >> /etc/hosts

# Update all software
echo "Running yum update...";
tput setaf ${color[default]};
yum makecache fast;
yum update -y;

## NTP No longer works DEVOPS-687
## Install and configure ntp; needed for Puppet cert signing
#tput setaf ${color[green]};
#echo "Installing and configuring ntp...";
#tput setaf ${color[default]};
#yum install -y ntp;
#chkconfig ntpd on;
#ntpdate pool.ntp.org;

# Install and configure chrony; needed for Puppet cert signing
service ntpd stop
chkconfig ntpd off
yum -y install chrony
systemctl enable chronyd.service
systemctl restart chronyd.service
chronyd -q 'pool pool.ntp.org iburst'

tput setaf ${color[green]};
echo "Installing Puppet...";
tput setaf ${color[default]};

# Add /usr/local/bin to root's path since puppet installs itself there
echo 'PATH=/usr/local/bin:$PATH' >> ~/.bashrc;

# Install Puppet
curl -k https://${puppetmaster_domain}:8140/packages/current/install.bash | bash;

sed -i 's|^bitbucket_username.*||g' /etc/puppetlabs/puppet/puppet.conf;
sed -i 's|^bitbucket_password.*||g' /etc/puppetlabs/puppet/puppet.conf;
sed -i 's|^daemonize.*||g' /etc/puppetlabs/puppet/puppet.conf;
sed -i 's|^environment.*||g' /etc/puppetlabs/puppet/puppet.conf;

echo "environment=${project}${puppet_env}
daemonize = false
bitbucket_username = ${bitbucket_username}
bitbucket_password = ${bitbucket_password}" >> /etc/puppetlabs/puppet/puppet.conf;
sed -i "s|certname = .*|certname = local-${project}-$(date +%s).${puppet_tld}|g" /etc/puppetlabs/puppet/puppet.conf;

# Disable Puppet as a service
chkconfig puppet off;
service puppet stop;

tput setaf ${color[green]};
echo "Running Puppet...";
tput setaf ${color[default]};
# Run Puppet
puppet agent -t;

if [[ $? != 0 && $? != 2 ]]; then
    tput setaf ${color[red]};
    echo "Puppet run failed.";

    tput setaf ${color[yellow]};
    read -r -p "Sometimes you may just need to run puppet a second time. Try again? [Y/n] " response
    response=${response,,}    # tolower
    if [[ $response =~ ^(no|n)$ ]]; then
        tput setaf ${color[red]};
        echo "Aborted.";
        tput setaf ${color[default]};
        exit 1;
    else
        tput setaf ${color[default]};
        echo "Attempting to run puppet again...";
        puppet agent -t;

        if [[ $? != 0 && $? != 2 ]]; then
            tput setaf ${color[red]};
            echo "Puppet run failed on second attempt. Aborting.";
            tput setaf ${color[default]};
            exit 1;
        fi
    fi
fi

tput setaf ${color[green]};
echo "Configuring git credentials...";
tput setaf ${color[default]};
# Configure Git credentials
git config --system user.name $full_name;
git config --system user.email $email;

tput setaf ${color[green]};
echo "Stripping Bitbucket credentials from /etc/puppetlabs/puppet/puppet.conf...";
tput setaf ${color[default]};
sed -i 's|^bitbucket_username.*||g' /etc/puppetlabs/puppet/puppet.conf;
sed -i 's|^bitbucket_password.*||g' /etc/puppetlabs/puppet/puppet.conf;

tput setaf ${color[green]};
echo "";
echo "Installation successful! Byaaaahhhh!!!!!";
tput setaf ${color[yellow]};
echo "";
echo "To install the app, run the following (Copy/paste each command individually):";
echo "";
echo "su dev; cd ~";



ls -la /home/dev/conductor/vendor/bin/conductor &>/dev/null;
if [[ $? == 0 ]]; then
    echo "conductor app:deploy --repo-reference master --snapshot production-scrubbed --databases -vvv";
    echo "conductor app:deploy --snapshot production-scrubbed --assets -vvv";
#  This does not look correct. Two snapshots?
#    echo "conductor app:deploy --snapshot production-scrubbed --snapshot production-scrubbed --assets -vvv";
else
    echo "devops app:install --all --no-assets -vvv";
    echo "devops app:refresh-assets --all -vvv";
fi

tput setaf ${color[default]};
echo ""
exit 0;
