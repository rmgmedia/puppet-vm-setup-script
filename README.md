Robofirm Puppet VM Setup Script
===============================

This script is intended to configure a clean CentOS 7 installation for
use as a Robofirm project VM tied to a Puppet configuration.
