#!/bin/bash

set -eu -o pipefail

declare -A color
color[default]=9
color[green]=2
color[red]=1
color[yellow]=3

if [[ "root" != "$(whoami)" ]]; then
  tput setaf ${color[red]}
  echo "This command must be run as root."
  tput setaf ${color[default]}
  exit 1
fi

if [[ "not a tty" == "$(tty)" ]]; then
  tput setaf ${color[red]}
  echo "This command must be run with an interactive shell."
  tput setaf ${color[default]}
  exit 1
fi

if [[ ! -f /usr/bin/git ]]; then
  tput setaf ${color[red]}
  echo "git not installed, installing for you"
  tput setaf ${color[default]}
  yum -y install git
fi

# Update all software
echo "Running yum update..."
tput setaf ${color[default]}
yum makecache fast
yum update -y

# Force sync chrony and ensure that it's enabled; needed for Puppet cert signing
systemctl stop chronyd.service
chronyd -q 'pool pool.ntp.org iburst'
systemctl enable --now chronyd.service


tput setaf ${color[green]}
echo "
Welcome to the RoboDocker VM Setup Script!
"

tput setaf ${color[default]}
echo "This script will set up the RoboDocker VM from a clean CentOS 7 installation.

We will ask you for some information about you and your project and then you will be on your way.

After you enter your project information, Puppet will run which may take some time.

Refer to this link for documentation:
https://rmgmedia.atlassian.net/wiki/spaces/RMGU/pages/129171953/RoboDocker+Development+Environment+Strategy

This installer can be run again if it fails.
"

puppet_tld=robofirm.net
puppetmaster_domain=newpuppet.robofirm.net

full_name=""
while [[ -z "$full_name" ]]; do
  tput setaf ${color[yellow]}
  echo "Your Full Name:"
  tput setaf ${color[default]}
  read -r full_name
  git config --global user.name \"$full_name\"
  if [[ -z "$full_name" ]]; then
    tput setaf ${color[red]}
    echo "Full Name must be set."
    tput setaf ${color[default]}
  fi
done

email=""
while [[ -z "$email" ]]; do
  tput setaf ${color[yellow]}
  echo "Email:"
  tput setaf ${color[default]}
  read -r email
  git config --global user.email \"$email\"
  if [[ -z "$email" ]]; then
    tput setaf ${color[red]}
    echo "Email must be set."
    tput setaf ${color[default]}
  fi
done

bitbucketCredentialsValid=0
while [[ 0 == "$bitbucketCredentialsValid" ]]; do
  bitbucket_username=""
  while [[ -z "$bitbucket_username" ]]; do
    tput setaf ${color[yellow]}
    echo "Bitbucket RMGMedia Username (not email):"
    tput setaf ${color[default]}
    read -r bitbucket_username
    if [[ -z "$bitbucket_username" ]]; then
      tput setaf ${color[red]}
      echo "Bitbucket Username must be set."
      tput setaf ${color[default]}
    fi
  done

  bitbucket_password=""
  while [[ -z "$bitbucket_password" ]]; do
    tput setaf ${color[yellow]}
    echo "Please create an App Password. Visit https://bitbucket.org/account/user/$bitbucket_username/app-passwords"
    echo "Click Create app password."
    echo "Give the app password a name related to the application that will use the password."
    echo "Select the specific access and permissions. Check only Account Email/Read/Write"
    echo "Copy the generated password and record it and copy/paste below. The password is only displayed this one time."
    echo "Bitbucket App Password (not login password):"
    tput setaf ${color[default]}
    read -rs bitbucket_password
    if [[ -z "$bitbucket_password" ]]; then
      tput setaf ${color[red]}
      echo "Bitbucket App Password must be set."
      tput setaf ${color[default]}
    fi
  done

  # Validate Bitbucket credentials
  echo "Validating Bitbucket credentials..."
  httpStatus=$(curl -s -o /dev/null -I -w "%{http_code}" --user "${bitbucket_username}:${bitbucket_password}" https://api.bitbucket.org/2.0/user)
  if [[ 200 == "$httpStatus" ]]; then
    bitbucketCredentialsValid=1
  else
    tput setaf ${color[red]}
    echo "Bitbucket credentials failed. Please re-enter."
    tput setaf ${color[default]}
    bitbucket_username=""
    bitbucket_password=""
  fi
done

tput setaf ${color[green]}
echo "Thank you. Your RoboDocker VM is now being set up..."
tput setaf ${color[default]}

# Set Hostname
hostnamectl set-hostname robodocker.${puppet_tld} --static
echo "127.0.0.1 robodocker" >>/etc/hosts
# Setting the hostname can break networking. This fixes it
#INTERFACE=$(ip addr | awk '/state UP/ {print $2}' | head -n 1 | sed 's/.$//')
#nmcli connection up $INTERFACE

# Update all software
echo "Running yum update..."
tput setaf ${color[default]}
yum makecache fast
yum update -y

# Force sync chrony and ensure that it's enabled; needed for Puppet cert signing
systemctl stop chronyd.service
chronyd -q 'pool pool.ntp.org iburst'
systemctl enable --now chronyd.service

# Install Puppet
tput setaf ${color[green]}
echo "Installing Puppet..."
tput setaf ${color[default]}

# Add /usr/local/bin to root's path since puppet installs itself there
sed -i "s|^PATH=/usr/local/bin:\$PATH||g" ~/.bashrc
echo "PATH=/usr/local/bin:\$PATH" >> ~/.bashrc

# Install Puppet
curl -k https://${puppetmaster_domain}:8140/packages/current/install.bash | bash

sed -i "s|^bitbucket_username.*||g" /etc/puppetlabs/puppet/puppet.conf
sed -i "s|^bitbucket_password.*||g" /etc/puppetlabs/puppet/puppet.conf
sed -i "s|^daemonize.*||g" /etc/puppetlabs/puppet/puppet.conf
sed -i "s|^environment.*||g" /etc/puppetlabs/puppet/puppet.conf
echo "environment=robodocker
daemonize = false
bitbucket_username = ${bitbucket_username}
bitbucket_password = ${bitbucket_password}" >>/etc/puppetlabs/puppet/puppet.conf
sed -i "s|certname = .*|certname = robodocker-$(date +%s).${puppet_tld}|g" /etc/puppetlabs/puppet/puppet.conf

# Disable Puppet as a service
systemctl disable --now puppet.service

# Run puppet
# Allow to continue of return status is 2 as this is a normal exit code for this command
puppet agent -t || test $? -eq 2

# @todo Most of the below should probably be moved into Puppet

# Configure Git credentials
tput setaf ${color[green]}
echo "Configuring git credentials..."
tput setaf ${color[default]}
runuser -l webuser -c "git config --global user.name \"$full_name\""
runuser -l webuser -c "git config --global user.email \"$email\""

# Open firewall for http, https, and samba
tput setaf ${color[green]}
echo "Updating firewall services..."
tput setaf ${color[default]}
enabledServices=$(firewall-cmd --zone=public --list-services)
serviceEnabled=0
if [[ ! $enabledServices =~ (^| )http($| ) ]]; then
  firewall-cmd --zone=public --add-service=http >/dev/null
  serviceEnabled=1
fi
if [[ ! $enabledServices =~ (^| )https($| ) ]]; then
  firewall-cmd --zone=public --add-service=https >/dev/null
  serviceEnabled=1
fi
if [[ ! $enabledServices =~ (^| )samba($| ) ]]; then
  firewall-cmd --zone=public --add-service=samba >/dev/null
  serviceEnabled=1
fi
if [[ 1 == "$serviceEnabled" ]]; then
  firewall-cmd --runtime-to-permanent >/dev/null
  firewall-cmd --reload >/dev/null
fi

# Reset terminal and exit 0
tput setaf ${color[green]}
echo "RoboDocker installation complete."
tput setaf ${color[default]}
echo ""
exit 0
